package src;

import java.util.Random;

import Quque.linkedList;
import objects.Car;

public class StopLight {
	
	linkedList<Car> northQuque;
	linkedList<Car> eastQuque;
	linkedList<Car> southQuque;
	linkedList<Car> westQuque;
	
	//Light Swtich acts as which lanes can go through (0 means vertical can go, 1 means horizontal)
	int lightSwitch=0;
	
	public StopLight(){
		
		northQuque= new linkedList<Car>();
		eastQuque= new linkedList<Car>();
		southQuque= new linkedList<Car>();
		westQuque= new linkedList<Car>();
		
	}
	public void cycle(){
			
		try{
			
			System.out.println("North car: " + northQuque.get(0) + " Time spent in Quque: " + northQuque.get(0).getExistanceTime() + " milliseconds " + "Cars in Quque: " + northQuque.getSize());
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the North");
		}
		
		try{
			
			System.out.println("East  car: " + eastQuque.get(0) + " Time spent in Quque: " + eastQuque.get(0).getExistanceTime() + " milliseconds" + "Cars in Quque: " + eastQuque.getSize());
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the East");
		}
	
		try{
			
			System.out.println("South car: " + southQuque.get(0) + " Time spent in Quque: " + southQuque.get(0).getExistanceTime() + " milliseconds" + "Cars in Quque: " + southQuque.getSize());
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the South");
		}
		
		try{
			
			System.out.println("West  car: " + westQuque.get(0) + " Time spent in Quque: " + westQuque.get(0).getExistanceTime() + " milliseconds" + "Cars in Quque: " + westQuque.getSize());
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the West");
		}

		System.out.println("___________________________________________________");
		
		if(lightSwitch==0){
			
			try{
				
				deQuque(northQuque);
			}catch(NullPointerException e){
				
				//Left empty because you cant deQuque an empty quque
			}
			
			try{
				
				deQuque(southQuque);
			}catch(NullPointerException e){
				
				//Left empty because you cant deQuque an empty quque
			}
			
			if(eastQuque.getSize()>5 || westQuque.getSize()>5){
				
				System.out.println("The light is now letting horizontal lanes passage");
				lightSwitch=1;
			}
		}else if(lightSwitch==1){
			
			try{
				
				deQuque(eastQuque);
			}catch(NullPointerException e){
				
				//Left empty because you cant deQuque an empty quque
			}
			
			try{
				
				deQuque(westQuque);
			}catch(NullPointerException e){
				
				//Left empty because you cant deQuque an empty quque
			}
			
			if(northQuque.getSize()>5 || southQuque.getSize()>5){
				
				System.out.println("The light is now letting vertical lanes passage");
				lightSwitch=0;
			}
		}else{
			
			System.out.println("Something went wrong with changing the lights(StopLight.java line 110)");
		}
	}
	
	public void enQuque(linkedList<Car> t, Car c){
		
		t.enQue(c);
	}
	
	public void deQuque(linkedList<Car> c){
	
		c.deQuque();
	}
	
	public void generateCarArrival(){
		
		Car c= makeCar();
		if(c.getPos().equals("North")){
			
			enQuque(northQuque, c);
		}else if(c.getPos().equals("East")){
			
			enQuque(eastQuque, c);
		}else if(c.getPos().equals("South")){
			
			enQuque(southQuque, c);
		}else{
			
			enQuque(westQuque, c);
		}
	}
	public Car makeCar(){
		
		Car c= new Car();
		Random r = new Random();
		int t= r.nextInt(4);
		if(t==0){
			
			c.setPos("North");
		}else if(t==1){
			
			c.setPos("East");
		}else if(t==2){
			
			c.setPos("South");
		}else if(t==3){
			
			c.setPos("West");
		}else{
			
			System.out.println("Something went wrong with setting the position(StopSign.java line 186)");
		}
		
		return c;
	}
}
