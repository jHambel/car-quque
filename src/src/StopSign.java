package src;

import java.util.Random;

import Quque.linkedList;
import objects.Car;

public class StopSign {

	linkedList<Car> northQuque;
	linkedList<Car> eastQuque;
	linkedList<Car> southQuque;
	linkedList<Car> westQuque;
	
	public StopSign(){
		
		northQuque= new linkedList<Car>();
		eastQuque= new linkedList<Car>();
		southQuque= new linkedList<Car>();
		westQuque= new linkedList<Car>();
		
	}
	
	//Checks all the cars in the quques, then dequques the earliest car(s) in the quque 
	public void cycle(){
		
		linkedList<Car> tempList= new linkedList<Car>();
		
		try{
			
			tempList.enQue(northQuque.get(0));
			System.out.println("North car: " + northQuque.get(0) + " Time spent in Quque: " + northQuque.get(0).getExistanceTime() + " milliseconds");
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the North");
		}
		
		try{
			
			tempList.enQue(eastQuque.get(0));
			System.out.println("East  car: " + eastQuque.get(0) + " Time spent in Quque: " + eastQuque.get(0).getExistanceTime() + " milliseconds");
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the East");
		}
	
		try{
			
			tempList.enQue(southQuque.get(0));
			System.out.println("South car: " + southQuque.get(0) + " Time spent in Quque: " + southQuque.get(0).getExistanceTime() + " milliseconds");
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the South");
		}
		
		try{
			
			tempList.enQue(westQuque.get(0));
			System.out.println("West  car: " + westQuque.get(0) + " Time spent in Quque: " + westQuque.get(0).getExistanceTime() + " milliseconds");
		}catch(NullPointerException e){
			
			System.out.println("There is no car in the West");
		}

		System.out.println("___________________________________________________");
		int tempInt=0;
		
		try{
			
			long greatestTime=tempList.get(0).getExistanceTime();

			for(int i=0;i<tempList.getSize()-1;i++){
			
				if(tempList.get(i+1).getExistanceTime()>greatestTime){
				
					System.out.print(tempList.get(i+1).getPos());
					tempInt=i+1;
					greatestTime=tempList.get(i+1).getExistanceTime();
				}
			}
			System.out.println(tempInt+ " " +tempList.get(tempInt));
			try{
			
				if(tempList.get(tempInt).getPos().equals("North")){
				
					deQuque(northQuque);
				}if(tempList.get(tempInt).getPos().equals("East")){
			
					deQuque(eastQuque);
				}else if(tempList.get(tempInt).getPos().equals("South")){
			
					deQuque(southQuque);
				}else if(tempList.get(tempInt).getPos().equals("West")){
			
					deQuque(westQuque);
				}
			}catch(NullPointerException e){
			
				try{
					if(tempList.get(tempInt).getPos().equals("East")){
					
						deQuque(eastQuque);
					}else if(tempList.get(tempInt).getPos().equals("South")){
					
						deQuque(southQuque);
					}else if(tempList.get(tempInt).getPos().equals("West")){
					
						deQuque(westQuque);
					}
				}catch(NullPointerException r){
				
					try{
					
						if(tempList.get(tempInt).getPos().equals("South")){
						
							deQuque(southQuque);
						}else if(tempList.get(tempInt).getPos().equals("West")){
						
							deQuque(westQuque);
						}
					}catch(NullPointerException t){
					
						try{
							if(tempList.get(tempInt).getPos().equals("West")){
							
								deQuque(westQuque);
							}
						}catch(NullPointerException y){
						
						}
					}
				}
			}
		}catch(NullPointerException e){
			
			System.out.println("There are no cars at any position");
		}
	}
	
	public void enQuque(linkedList<Car> t, Car c){
		
		t.enQue(c);
	}
	
	public void deQuque(linkedList<Car> c){
	
		c.deQuque();
	}
	
	public void generateCarArrival(){
		
		Car c= makeCar();
		if(c.getPos().equals("North")){
			
			enQuque(northQuque, c);
		}else if(c.getPos().equals("East")){
			
			enQuque(eastQuque, c);
		}else if(c.getPos().equals("South")){
			
			enQuque(southQuque, c);
		}else{
			
			enQuque(westQuque, c);
		}
	}
	public Car makeCar(){
		
		Car c= new Car();
		Random r = new Random();
		int t= r.nextInt(4);
		if(t==0){
			
			c.setPos("North");
		}else if(t==1){
			
			c.setPos("East");
		}else if(t==2){
			
			c.setPos("South");
		}else if(t==3){
			
			c.setPos("West");
		}else{
			
			System.out.println("Something went wrong with setting the position(StopSign.java line 186)");
		}
		
		return c;
	}
}
