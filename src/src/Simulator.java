package src;

import java.util.Random;

import objects.Car;

public class Simulator {

	public static void main(String[] args){
		
		StopSign ss= new StopSign();
		StopLight sl= new StopLight();
		Random r= new Random();
		int on=2;
		
		//Stop Sign Test
		while(on==1){
			
			if(r.nextInt(2)==1){
				ss.generateCarArrival();
			}
			
			ss.cycle();
		}
		
		//Stop Light Test
		while(on==2){
			
			if(r.nextInt(2)==1){
				sl.generateCarArrival();
			}
			
			sl.cycle();
		}
	}
}
